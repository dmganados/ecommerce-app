import { Form, Button, Navbar, Nav, NavDropdown, InputGroup, NavLink} from 'react-bootstrap';

export default function NavItems() {
    return(
        <>
        <Navbar>
            <NavLink>Home</NavLink>
            <NavLink>Inventory</NavLink>
            <NavLink>Orders</NavLink>
            <NavLink>Reports</NavLink>
            <NavLink>Shipment</NavLink>
        </Navbar>
        </>
    )
}