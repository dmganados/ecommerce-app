import { Form, Button, Navbar, Nav, NavDropdown, InputGroup, NavLink, Container} from 'react-bootstrap';
import { useState } from 'react'
import NavItems from './NavMenu';

export default function Navigation() {


    return(
        <>
        <div className="navDiv">
            {/* Search section */}
            <InputGroup className='searchBar'>
                <Form.Control type='text' placeholder='Search something...' className='inputSearch' />  
                <Button className='searchBtn'>
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" className="bi bi-search magnifier" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>  
                </Button>            
            </InputGroup>

            <Navbar className='topNav'>
                {/* Message section */}
                    <Nav.Link href='/message' className='msgNavLink'>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" className="bi bi-chat-left-dots-fill message" viewBox="0 0 16 16">
                    <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H4.414a1 1 0 0 0-.707.293L.854 15.146A.5.5 0 0 1 0 14.793V2zm5 4a1 1 0 1 0-2 0 1 1 0 0 0 2 0zm4 0a1 1 0 1 0-2 0 1 1 0 0 0 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                    </svg>
                    </Nav.Link>
                    
                    {/* Avatar, Profile, and Logout section */}
                    <NavDropdown  title='DG' className='profileLink' noCaret >
                        <NavDropdown.Item className='dropdown' href='/profile' >Profile</NavDropdown.Item>
                        <NavDropdown.Item className='dropdown'>Logout</NavDropdown.Item>
                    </NavDropdown> 
            </Navbar>
        </div>

            {/* Navigation Items */}
            <Navbar className='menuItems'>
                <NavLink href='/' className='home'>Home</NavLink>
                <NavLink className='inventory'>Inventory</NavLink>
                <NavLink className='orders'>Orders</NavLink>
                <NavLink className='reports'>Reports</NavLink>
                <NavLink className='shipment'>Shipment</NavLink>
            </Navbar>        
        </>
    )
}