import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './seller-pages/Home'
import Profile from './seller-pages/profile/Profile';
import Message from './seller-pages/message/Message';

function App() {
  return (
   <BrowserRouter>
   <Routes>
    <Route path="/" element={<Home />} />
    <Route path='/profile' element={<Profile />} />
    <Route path='/message' element={<Message />} />
   </Routes>
   </BrowserRouter>
  )
}

export default App;
